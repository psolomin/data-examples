SHELL := /bin/bash

build:
	sbt assembly

clean:
	rm -rf outputs

style:
	sbt scalastyle

test: style
	{ \
		export JAVA_HOME=$$(/usr/libexec/java_home -v 1.8) ;\
		sbt test ;\
	}

up-kafka:
	docker-compose up --build topics

populate-kafka-topics: up-kafka
	kafkacat -b localhost:19092 -t app-installs -K: -T -P -l inputs/installs-example.txt
	kafkacat -b localhost:19092 -t app-transactions -K: -T -P -l inputs/transactions-example.txt

clean-env: clean
	docker-compose down -v
