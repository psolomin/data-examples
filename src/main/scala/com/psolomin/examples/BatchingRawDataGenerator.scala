package com.psolomin.examples

import com.psolomin.examples.generators.{BatchingRawDataGeneratorProgram, InstallsRecordsGeneratorConfig, TransactionsRecordsGeneratorConfig}
import org.apache.spark.sql.SparkSession

object BatchingRawDataGenerator {
  def parseArgs(args: Array[String]): (String, Long, String, Long) = {
    var installsTable = ""
    var installsLineCount = 0L
    var transactionsTable = ""
    var transactionsLineCount = 0L
    args.sliding(2, 2).toList.collect {
      case Array("--installs-table", argInstallsTable: String) =>
        installsTable = argInstallsTable

      case Array("--installs-lines-count", argInstallsLinesCount: String) =>
        installsLineCount = argInstallsLinesCount.toLong

      case Array("--transactions-table", argTransactionsTable: String) =>
        transactionsTable = argTransactionsTable

      case Array("--transactions-lines-count", argTransactionsLineCount: String) =>
        transactionsLineCount = argTransactionsLineCount.toLong
    }
    (installsTable, installsLineCount, transactionsTable, transactionsLineCount)
  }

  def main(args: Array[String]): Unit = {
    val (installsTable, installsLineCount, transactionsTable, transactionsLineCount) = parseArgs(args)

    val distinctUserCountriesMax = 5
    val distinctAppIdsMax = 25
    val distinctOsMax = 7
    val distinctProductIdsMax = 30
    val maxTransactionValue = 100
    val minDay = 10000
    val maxDay = 10050

    val installsConfig = InstallsRecordsGeneratorConfig(
      distinctUserCountriesMax,
      distinctAppIdsMax,
      distinctOsMax,
      minDay,
      maxDay
    )

    val transactionsConfig = TransactionsRecordsGeneratorConfig(
      installsLineCount,
      distinctAppIdsMax,
      distinctProductIdsMax,
      maxTransactionValue,
      minDay,
      maxDay
    )

    val spark = SparkSession.builder()
      .config("fs.s3.canned.acl", "BucketOwnerFullControl")
      .config("spark.sql.sources.partitionOverwriteMode", "static")
      .config("spark.hadoop.hive.exec.dynamic.partition", "true")
      .config("spark.hadoop.hive.exec.dynamic.partition.mode", "nonstrict")
      .enableHiveSupport()
      .getOrCreate()

    BatchingRawDataGeneratorProgram.run(
      spark,
      installsConfig,
      installsLineCount,
      installsTable,
      transactionsConfig,
      transactionsLineCount,
      transactionsTable
    )
  }
}
