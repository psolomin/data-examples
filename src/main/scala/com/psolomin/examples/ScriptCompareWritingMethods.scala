package com.psolomin.examples

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

object ScriptCompareWritingMethods {
  /*
  Quick check of speed of different writing methods.
  Data size - 15.7GB
   */
  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000000 + "s")
    result
  }

  val spark = SparkSession.builder().
    config("fs.s3.canned.acl", "BucketOwnerFullControl").
    config("spark.sql.sources.partitionOverwriteMode", "static").
    config("spark.hadoop.hive.exec.dynamic.partition", "true").
    config("spark.hadoop.hive.exec.dynamic.partition.mode", "nonstrict").
    enableHiveSupport().
    getOrCreate()

  val df = spark.read.parquet(
    "s3://org-dev-us-east-1-toy-data-platform-data/toy_data_platform/transactions/"
  )

  df.repartition(col("p_date")).cache()
  df.select("product_id").distinct()

  time {
    df.write.mode("overwrite").
      parquet("s3://org-dev-us-east-1-toy-data-platform-data/toy_data_platform/transactions_baseline/")
  } // 77s

  time {
    df.write.mode("overwrite").partitionBy("p_date").
      parquet("s3://org-dev-us-east-1-toy-data-platform-data/toy_data_platform/transactions_partitioned/")
  } // 107s

  time {
    df.createOrReplaceTempView("def")
    spark.sql(
      """
        |insert overwrite table toy_data_platform.test
        |partition (p_date) select * from def
        |""".stripMargin
    )
  } // 139s
}
