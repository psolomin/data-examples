package com.psolomin.examples.generators

import java.sql.Timestamp
import java.time.{Instant, ZoneOffset}
import java.util.concurrent.{ThreadLocalRandom, TimeUnit}

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.StructType

object GeneratorHelper {
  val MAX_GENERATED_SIZE: Long = Long.MaxValue / 2

  def doGenerate(spark: SparkSession, schema: StructType, linesCount: Long, generatorFunc: Long => Row): DataFrame = {
    if (linesCount > MAX_GENERATED_SIZE) {
      val msg = s"Generating datasets bigger than $MAX_GENERATED_SIZE is not allowed"
      throw new Exception(msg)
    }
    val rows = spark.range(0L, linesCount).rdd.map(id => generatorFunc(id))
    spark.createDataFrame(rows, schema)
  }

  def generateTimeFields(tsMinDay: Int, tsMaxDay: Int): (Timestamp, String) = {
    val timeSeed = TimeUnit.DAYS.toMillis(tsMinDay) +
      ThreadLocalRandom.current().nextLong(TimeUnit.DAYS.toMillis(tsMaxDay - tsMinDay))

    val tsUtc = Instant.ofEpochMilli(timeSeed).atZone(ZoneOffset.UTC).toLocalDateTime
    val ts = java.sql.Timestamp.valueOf(tsUtc)
    val d = java.sql.Date.valueOf(tsUtc.toLocalDate).toString
    (ts, d)
  }
}
