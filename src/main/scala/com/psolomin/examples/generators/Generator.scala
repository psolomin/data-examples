package com.psolomin.examples.generators

import org.apache.spark.sql.{DataFrame, SparkSession}

trait Generator {
  def generate(sparkSession: SparkSession, linesCount: Long): DataFrame
}
