package com.psolomin.examples.generators

import org.apache.spark.sql.SparkSession
import java.util.concurrent.CountDownLatch

import scala.util.Random

class GeneratorRunner(
  latch: CountDownLatch,
  spark: SparkSession,
  generator: Generator,
  table: String,
  lines: Long
) extends Runnable {
  def run(): Unit = {
    val newSession = spark.newSession()

    val df = generator.generate(newSession, lines)
    val tempView = Random.alphanumeric.filter(!_.isDigit).take(8).mkString

    df.createOrReplaceTempView(tempView)
    newSession.sql(
      s"""
         |insert overwrite table $table
         |partition (p_date) select * from $tempView
         |""".stripMargin)

    latch.countDown()
  }
}

object BatchingRawDataGeneratorProgram {
  def run(
    spark: SparkSession,
    installsConfig: InstallsRecordsGeneratorConfig,
    installsLinesCount: Long,
    installsTableName: String,
    transactionsConfig: TransactionsRecordsGeneratorConfig,
    transactionsLinesCount: Long,
    transactionsTableName: String
  ): Unit = {
    val latch = new CountDownLatch(2)

    val installsGenerator = InstallsRecordsGenerator(installsConfig)
    val transactionsRecordsGenerator = TransactionsRecordsGenerator(transactionsConfig)

    new Thread(new GeneratorRunner(
      latch, spark, installsGenerator, installsTableName, installsLinesCount)
    ).start()

    new Thread(new GeneratorRunner(
      latch, spark, transactionsRecordsGenerator, transactionsTableName, transactionsLinesCount)
    ).start()

    latch.await()
  }
}
