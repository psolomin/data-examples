package com.psolomin.examples.generators

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType, TimestampType}

case class InstallsRecordsGeneratorConfig(
  distinctUserCountriesMax: Int,
  distinctAppIdsMax: Int,
  distinctOsMax: Int,
  tsMinDay: Int,
  tsMaxDay: Int
) extends Config

case class InstallsRecordsGenerator(
    config: InstallsRecordsGeneratorConfig
) extends Generator {

  val schema: StructType = StructType(List(
      StructField("user_id", StringType, nullable = true),
      StructField("country", StringType, nullable = true),
      StructField("installed_app_id", StringType, nullable = true),
      StructField("os", StringType, nullable = true),
      StructField("ts", TimestampType, nullable = true),
      StructField("p_date", StringType, nullable = true)
    )
  )

  def strFieldsGenerator(rowIndex: Long): Row = {
    val userId = f"user-${rowIndex}%09d"
    val appId = f"application-${rowIndex % config.distinctAppIdsMax}%09d"
    val os = f"Android-${rowIndex % config.distinctOsMax}%09d"
    val userCountry = f"country-${rowIndex % config.distinctUserCountriesMax}%09d"
    val (ts, d) = GeneratorHelper.generateTimeFields(config.tsMinDay, config.tsMaxDay)
    Row(userId, userCountry, appId, os, ts, d)
  }

  def generate(sparkSession: SparkSession, linesCount: Long): DataFrame =
    GeneratorHelper.doGenerate(sparkSession, this.schema, linesCount, this.strFieldsGenerator)
      .repartition(col("p_date"), col("installed_app_id"))
}
