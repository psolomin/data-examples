package com.psolomin.examples.generators

import java.util.concurrent.ThreadLocalRandom

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{DecimalType, StringType, StructField, StructType, TimestampType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

case class TransactionsRecordsGeneratorConfig(
  distinctUserIdsMax: Long,
  distinctAppIdsMax: Int,
  distinctProductIdsMax: Int,
  maxTransactionValue: Int,
  tsMinDay: Int,
  tsMaxDay: Int
) extends Config

case class TransactionsRecordsGenerator(
  config: TransactionsRecordsGeneratorConfig
) extends Generator {

  val schema: StructType = StructType(List(
      StructField("id", StringType, nullable = true),
      StructField("user_id", StringType, nullable = true),
      StructField("app_id", StringType, nullable = true),
      StructField("product_id", StringType, nullable = true),
      StructField("value", DecimalType(10, 2), nullable = true),
      StructField("currency", StringType, nullable = true),
      StructField("ts", TimestampType, nullable = true),
      StructField("p_date", StringType, nullable = true)
    )
  )

  def strFieldsGenerator(rowIndex: Long): Row = {
    val id = f"transaction-${rowIndex}%09d"
    val userId = f"user-${rowIndex % config.distinctUserIdsMax}%09d"
    val appId = f"application-${rowIndex % config.distinctAppIdsMax}%09d"
    val productId = f"product-${rowIndex % config.distinctProductIdsMax}%09d"
    val value = BigDecimal(ThreadLocalRandom.current().nextInt(config.maxTransactionValue))
    val currency = "EUR"
    val (ts, d) = GeneratorHelper.generateTimeFields(config.tsMinDay, config.tsMaxDay)
    Row(id, userId, appId, productId, value, currency, ts, d)
  }

  def generate(sparkSession: SparkSession, linesCount: Long): DataFrame =
    GeneratorHelper.doGenerate(sparkSession, this.schema, linesCount, this.strFieldsGenerator)
      .repartition(col("p_date"), col("app_id"))
}
