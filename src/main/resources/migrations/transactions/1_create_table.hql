create external table if not exists toy_data_platform.transactions (
    id string,
    user_id string,
    app_id string,
    product_id string,
    value decimal(10, 2),
    currency string,
    ts timestamp
)
partitioned by (p_date string)
stored as parquet
tblproperties ("parquet.compression"="SNAPPY");
