create external table if not exists toy_data_platform.installs (
    user_id string,
    country string,
    installed_app_id string,
    os string,
    ts timestamp
)
partitioned by (p_date string)
stored as parquet
tblproperties ("parquet.compression"="SNAPPY");
