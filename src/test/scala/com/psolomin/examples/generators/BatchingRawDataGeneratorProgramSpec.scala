package com.psolomin.examples.generators

import java.time.{Instant, ZoneOffset}
import java.util.concurrent.TimeUnit

import com.holdenkarau.spark.testing.DatasetSuiteBase
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{max, min}
import org.scalatest.{BeforeAndAfter, FunSuite}

class BatchingRawDataGeneratorProgramSpec extends FunSuite with DatasetSuiteBase with BeforeAndAfter {
  val dbName = "test"
  val installsTableName = "installs"
  val installsDbAndTable = s"$dbName.$installsTableName"
  val transactionsTableName = "transactions"
  val transactionsDbAndTable = s"$dbName.$transactionsTableName"

  val installsLinesCount = 10
  val transactionsLinesCount = 100
  val distinctUserCountriesMax = 5
  val distinctAppIdsMax = 6
  val distinctOsMax = 7
  val distinctProductIdsMax = 30
  val maxTransactionValue = 100
  val minDay = 10000
  val maxDay = 10050

  before {
    spark.sql(s"create database $dbName")
    spark.sql(
      s"""create table $installsDbAndTable
         |(
         |    user_id string,
         |    country string,
         |    installed_app_id string,
         |    os string,
         |    ts timestamp,
         |    p_date date
         |)
         |using parquet
         |partitioned by (p_date)
         |""".stripMargin)
    spark.sql(
      s"""create table $transactionsDbAndTable
         |(
         |    id string,
         |    user_id string,
         |    app_id string,
         |    product_id string,
         |    value decimal(10, 2),
         |    currency string,
         |    ts timestamp,
         |    p_date date
         |)
         |using parquet
         |partitioned by (p_date)
         |""".stripMargin)
  }

  after {
    spark.sql(s"drop table $installsDbAndTable")
    spark.sql(s"drop table $transactionsDbAndTable")
    spark.sql(s"drop database $dbName")
  }

  test("generates new data frames") {
    val installsConfig = InstallsRecordsGeneratorConfig(
      distinctUserCountriesMax,
      distinctAppIdsMax,
      distinctOsMax,
      minDay,
      maxDay
    )

    val transactionsConfig = TransactionsRecordsGeneratorConfig(
      installsLinesCount,
      distinctAppIdsMax,
      distinctProductIdsMax,
      maxTransactionValue,
      minDay,
      maxDay
    )

    BatchingRawDataGeneratorProgram.run(
      spark,
      installsConfig,
      installsLinesCount,
      installsDbAndTable,
      transactionsConfig,
      transactionsLinesCount,
      transactionsDbAndTable
    )

    // check installs table:
    val actualInstalls = spark.sql(s"select * from $installsDbAndTable")
    checkInstallsTable(actualInstalls)


    // checking transactions table:
    val actualTransactions = spark.sql(s"select * from $transactionsDbAndTable")
    checkTransactionsTable(actualTransactions)
  }

  private def checkInstallsTable(df: DataFrame): Unit = {
    assert(df.count == installsLinesCount)

    assert(df.agg(min("user_id")).collect()(0).getString(0) == "user-000000000")
    assert(df.agg(max("user_id")).collect()(0).getString(0) == "user-000000009")
    assert(df.agg(min("country")).collect()(0).getString(0) == "country-000000000")
    assert(df.agg(max("country")).collect()(0).getString(0) == "country-000000004")
    assert(df.agg(min("installed_app_id")).collect()(0).getString(0) == "application-000000000")
    assert(df.agg(max("installed_app_id")).collect()(0).getString(0) == "application-000000005")
    assert(df.agg(min("os")).collect()(0).getString(0) == "Android-000000000")
    assert(df.agg(max("os")).collect()(0).getString(0) == "Android-000000006")

    checkDateLikeFields(df)
  }

  private def checkTransactionsTable(df: DataFrame): Unit = {
    assert(df.count == transactionsLinesCount)

    assert(df.agg(min("user_id")).collect()(0).getString(0) == "user-000000000")
    assert(df.agg(max("user_id")).collect()(0).getString(0) == "user-000000009")
    assert(df.agg(min("app_id")).collect()(0).getString(0) == "application-000000000")
    assert(df.agg(max("app_id")).collect()(0).getString(0) == "application-000000005")

    assert(df.agg(min("product_id")).collect()(0).getString(0) == "product-000000000")
    assert(df.agg(max("product_id")).collect()(0).getString(0) == "product-000000029")

    checkDateLikeFields(df)
  }

  private def checkDateLikeFields(df: DataFrame): Unit = {
    val minTs = df.agg(min("ts")).collect()(0).getTimestamp(0)
    val maxTs = df.agg(max("ts")).collect()(0).getTimestamp(0)
    val minDate = df.agg(min("p_date")).collect()(0).getDate(0)
    val maxDate = df.agg(max("p_date")).collect()(0).getDate(0)

    val lowerDt = Instant.ofEpochMilli(TimeUnit.DAYS.toMillis(minDay))
      .atZone(ZoneOffset.UTC).toLocalDateTime
    val upperDt = Instant.ofEpochMilli(TimeUnit.DAYS.toMillis(maxDay))
      .atZone(ZoneOffset.UTC).toLocalDateTime

    val tsLowerBound = java.sql.Timestamp.valueOf(lowerDt)
    val tsUpperBound = java.sql.Timestamp.valueOf(upperDt)

    val dateLowerBound = java.sql.Date.valueOf(lowerDt.toLocalDate)
    val dateUpperBound = java.sql.Date.valueOf(upperDt.toLocalDate)


    assert(!minTs.before(tsLowerBound))
    assert(!maxTs.after(tsUpperBound))

    assert(!minDate.before(dateLowerBound))
    assert(!maxDate.after(dateUpperBound))
  }
}
