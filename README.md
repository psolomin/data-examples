# Toy Data Platform

This repo aims to provide a set of self-contained examples of what can be called a cloud-based "Data Platform".

I also use this for running some experiments - load-testing, integration testing, proof-of-concept exercises, etc.

## Features

- Examples of infra & code for [batch processing](#batch-processing)
- Examples of infra & code for [stream processing](#stream-processing) (WIP)
- Examples of how both may be [connected together](#stream-batch-bridges) (WIP)

All examples aim to showcase three typical layers of what usually happens inside data platforms:
- Data ingestion (how data comes to transformers)
- Data transformation (this may consist of multiple layers)
- Access to transformed data

- Infra: AWS (more [here](#create-aws-infra))

## Disclaimer

- This repo **is not** a "best-practices" example of Terraform-ing and infra management
- This repo **is not** an example of "production grade" infra setup & application code 
- This repo **is not** a guide for building "Data Platforms" for any particular real-world use-case
  - real "Data Platforms" begin with some requirements in mind, while all the choices of components & approaches in this repo were more driven by my own curiosity, and may be suboptimal  
- This repo **is not** a collection of snippets of real-world things I've seen called "Data Platforms"
  - actually, quite the opposite - I've never seen similarly organized "Data Platforms", this repo is just my interpretation 

## Batch Processing

### Architecture

- Data ingestion
  - Data is assumed to be available in AWS S3. Data Generator is used to populate "ingested" datasets
- Data transformation (this may consist of multiple layers)
  - S3 -> transform (Spark) -> S3
- Access to transformed data
  - Reading from S3 using Presto
  - Loading to Redshift from S3

![batch-processing](docs/images/batch-arch.png)

### Plug and Play

After [infra is created](#create-aws-infra), get the public DNS of the Presto master node, and connect:

```
$ ./presto --server <master node DNS>:8889 --catalog hive
```

```
presto> select table_schema, table_name from information_schema.tables;
    table_schema    |    table_name    
--------------------+------------------
 ...
 toy_data_platform  | transactions     
 toy_data_platform  | installs         
```

**NOTE:** you might need to edit Security groups of the master node such that you could connect to `8889`.

## Stream Processing

### Architecture

WIP

### Plug and Play

WIP

## Stream-Batch Bridges

### Architecture

WIP

### Plug and Play

WIP

## Requirements

The setup has been tested with this machine & software:

```
$ uname -srpm
Darwin 18.7.0 x86_64 i386

$ make -v
GNU Make 3.81

$ sbt --version
sbt version in this project: 1.3.3
sbt script version: 1.3.3

$ docker-compose --version
docker-compose version 1.25.4, build 8d51620a

$ docker --version
Docker version 19.03.8, build afacb8b

$ kafkacat -V
kafkacat - Apache Kafka producer and consumer tool
https://github.com/edenhill/kafkacat
Copyright (c) 2014-2019, Magnus Edenhill
Version 1.5.0 (JSON, librdkafka 1.2.1 builtin.features=gzip,snappy,ssl,sasl,regex,lz4,sasl_gssapi,sasl_plain,sasl_scram,plugins,zstd,sasl_oauthbearer)

$ terraform version
Terraform v0.12.26

$ aws --version
aws-cli/2.0.39 Python/3.8.5

$ java -version
openjdk version "1.8.0_262"
OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_262-b10)
OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.262-b10, mixed mode)
```

If you want to build / test without Docker, make sure your host Scala & Java versions match the project's version.

You can use [asdf](https://github.com/asdf-vm/asdf) to manage multiple Scala versions:

```
# add plugin:
$ asdf plugin-add scala https://github.com/sylph01/asdf-scala.git

# install:
$ asdf install scala 2.11.12

# apply scala for current directory:
$ asdf local scala 2.11.12
```

## Dev Scripts

Run tests

```bash
make test
```

Play with local Spark shell

```
sbt console
```

```
scala>
scala> import com.psolomin.examples.generators.BatchingRawDataGeneratorProgram
...
scala> sys.exit
```

## Build artifacts for AWS EMR

```bash
make build
```

It will generate a jar at `./target/scala-2.11/simple-project_2.11-1.0.jar`

### Create AWS infra

Make sure you have access to your infra via AWS CLI, and that your `<your-profile>` in the Terraform files is set correctly.

Infra scripts expect all artifacts to be ready, so you must [build all artifacts](#build-artifacts-for-aws-emr) first.

After it's done, please, follow the instructions at [here](infra/README.md)

## Local Setup

WIP
