# Toy Data Platform - AWS Infrastructure

This dir holds infra-related scripts.

## Prepare artifacts

Before starting infra, please, follow instructions in the [root README](../README.md#build-artifacts-for-aws-emr)

## Prepare AMIs

```
$ make build-images
```

## Create infra

Infra consists of multiple segments:

```
$ tree -dL 1  
.
├── processing_batch_data_setup
├── processing_middleware
├── storage
...
```

There are explicit dependencies between them:

`processing_batch_data_setup` _depends on_ `processing_middleware` _depends on_ `storage`

Hence, the order of creating these segments will be:

1. `storage`
1. `processing_middleware`
1. `processing_batch_data_setup`

A segment can be created as follows:

```
$ cd storage
$ terraform init
$ terraform plan
$ terraform apply
```

To destroy a segment:

```
$ cd processing_middleware
$ terraform destroy
```
