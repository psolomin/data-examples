module vars {
  source = "../vars"
}

terraform {
  required_version = "~> 0.12"
  required_providers {
    aws = {
      version = "~> 3.7"
    }
    random = {
      version = "~> 2.3"
    }
  }
}

provider "aws" {
  profile = module.vars.profile
  region  = module.vars.region
}

module "bucket_data" {
  source           = "../modules/bucket"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  application_name = "data"
  tags             = module.vars.common_tags
}

module "bucket_artifacts" {
  source           = "../modules/bucket"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  application_name = "artifacts"
  tags             = module.vars.common_tags
}

module "bucket_processing_cluster_logs" {
  source           = "../modules/bucket"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  application_name = "processing-cluster-logs"
  tags             = module.vars.common_tags
}

module "bucket_metastore_service" {
  source           = "../modules/bucket"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  application_name = "metastore-service"
  tags             = module.vars.common_tags
}
