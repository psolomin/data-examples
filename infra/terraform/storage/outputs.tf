output "artifacts_storage" {
  value       = module.bucket_artifacts
  description = "Resource for artifacts' storage"
}

output "metastore_storage" {
  value       = module.bucket_metastore_service
  description = "Resource for storage of Metastore data"
}

output "logs_storage" {
  value       = module.bucket_processing_cluster_logs
  description = "Resource for logs' storage"
}

output "data_storage" {
  value       = module.bucket_data
  description = "Resource for artifacts' storage"
}
