module vars {
  source = "../vars"
}

locals {
  migrations_dir = "${path.module}/../../../src/main/resources/migrations"
  spark_jobs_jar = "${path.module}/../../../target/scala-2.11/spark-jobs.jar"

  mock_data = {
    installs_lines_count     = "200000000",
    transactions_lines_count = "800000000"
  }
}

terraform {
  required_version = "~> 0.12"
  required_providers {
    aws = {
      version = "~> 3.7"
    }
    random = {
      version = "~> 2.3"
    }
  }
}

provider "aws" {
  profile = module.vars.profile
  region  = module.vars.region
}

data "terraform_remote_state" "storage" {
  backend = "local"

  config = {
    path = "../storage/terraform.tfstate"
  }
}

data "terraform_remote_state" "processing_middleware" {
  backend = "local"

  config = {
    path = "../processing_middleware/terraform.tfstate"
  }
}

/*
Push applications' artifacts
*/

resource "aws_s3_bucket_object" "upload_migrations_scripts" {
  for_each = fileset(local.migrations_dir, "**")
  bucket   = data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id
  key      = "migrations/${each.value}"
  source   = "${local.migrations_dir}/${each.value}"
  etag     = filemd5("${local.migrations_dir}/${each.value}")
}

resource "aws_s3_bucket_object" "upload_spark_jobs" {
  bucket = data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id
  key    = "spark-jobs/${basename(local.spark_jobs_jar)}"
  source = local.spark_jobs_jar
  etag   = filemd5(local.spark_jobs_jar)
}

/*
Run data setup scripts
*/

module "cluster_for_schema_migrations" {
  source                        = "../modules/processing_cluster"
  org                           = module.vars.org
  project_name                  = module.vars.project_name
  env                           = module.vars.env
  region                        = module.vars.region
  tags                          = module.vars.common_tags
  application_name              = "schema-migrations"
  applications                  = ["Hive", "Spark", "Presto"]
  metastore_host                = data.terraform_remote_state.processing_middleware.outputs.metastore.host
  metastore_port                = data.terraform_remote_state.processing_middleware.outputs.metastore.port
  vpc_id                        = data.terraform_remote_state.processing_middleware.outputs.vpc.id
  subnet_id                     = data.terraform_remote_state.processing_middleware.outputs.vpc.subnet_ids[0]
  emr_role                      = data.terraform_remote_state.processing_middleware.outputs.roles.emr_roles.emr_role
  ec2_profile                   = data.terraform_remote_state.processing_middleware.outputs.roles.emr_roles.emr_ec2_profile
  emr_autoscaling_role          = data.terraform_remote_state.processing_middleware.outputs.roles.emr_roles.emr_autoscaling_role
  log_base_uri                  = "s3://${data.terraform_remote_state.storage.outputs.logs_storage.bucket_id}"
  key_name                      = module.vars.ssh_key_name
  additional_security_group_ids = [data.terraform_remote_state.processing_middleware.outputs.security_group_ssh.id]
  instance_type                 = "m5.4xlarge"
  min_workers                   = 3
  max_workers                   = 3

  steps = [
    {
      name : "create-database",
      jar : "command-runner.jar",
      args : [
        "hive", "-v", "-f", "s3://${data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id}/migrations/create_database.hql"
      ]
    },
    {
      name : "create-installs-table",
      jar : "command-runner.jar",
      args : [
        "hive", "-v", "-f", "s3://${data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id}/migrations/installs/1_create_table.hql"
      ]
    },
    {
      name : "create-transactions-table",
      jar : "command-runner.jar",
      args : [
        "hive", "-v", "-f", "s3://${data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id}/migrations/transactions/1_create_table.hql"
      ]
    },
    {
      /*
      spark-submit --master yarn --deploy-mode cluster \
        --conf spark.yarn.maxAppAttempts=1 \
        --conf spark.scheduler.mode=FAIR \
        --class com.psolomin.examples.BatchingRawDataGenerator \
        s3://org-dev-us-east-1-toy-data-platform-artifacts/spark-jobs/spark-jobs.jar \
        --installs-table toy_data_platform.installs --installs-lines-count 200000000  \
        --transactions-table toy_data_platform.transactions --transactions-lines-count 800000000
      */
      name : "populate-tables",
      jar : "command-runner.jar",
      args : [
        "spark-submit",
        "--master", "yarn",
        "--deploy-mode", "cluster",
        "--conf", "spark.yarn.maxAppAttempts=2",
        "--conf", "spark.scheduler.mode=FAIR",
        "--class", "com.psolomin.examples.BatchingRawDataGenerator",
        "s3://${data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_id}/spark-jobs/spark-jobs.jar",
        "--installs-table", "toy_data_platform.installs",
        "--installs-lines-count", local.mock_data.installs_lines_count,
        "--transactions-table", "toy_data_platform.transactions",
        "--transactions-lines-count", local.mock_data.transactions_lines_count
      ]
    }
  ]
}
