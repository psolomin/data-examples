output "org" {
  value = "org"
}

output "env" {
  value = "dev"
}

output "project_name" {
  value = "toy-data-platform"
}

output "profile" {
  value = "p-global-admin"
}

output "region" {
  value = "us-east-1"
}

output "main_cidr" {
  value = "10.0.0.0/16"
}

output "subnets" {
  value = {
    "10.0.0.0/24" : "us-east-1a",
    "10.0.1.0/24" : "us-east-1b",
    "10.0.2.0/24" : "us-east-1c"
  }
}

output "ssh_key_name" {
  value = "toy-data-platform-key"
}

output "common_tags" {
  value = {
    Org         = "org"
    Environment = "dev"
    ProjectName = "toy-data-platform"
    Operator    = "terraform"
  }
}

output "terraform_required_version" {
  value = "~> 0.12"
}

output "terraform_provider_aws_version" {
  value = "~> 3.7"
}

output "terraform_provider_random_version" {
  value = "~> 2.3"
}
