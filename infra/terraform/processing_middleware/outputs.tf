output "metastore" {
  value = module.hive_metastore_instances_group
}

output "vpc" {
  value = module.vpc_main
}

output "roles" {
  value = module.roles
}

output "security_group_ssh" {
  value = aws_security_group.ssh_from_internet
}
