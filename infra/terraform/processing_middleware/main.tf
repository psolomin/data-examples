module vars {
  source = "../vars"
}

locals {
  applications = {
    hive_metastore_service     = "hms"
    hive_metastore_provisioner = "hmp"
  }
}

terraform {
  required_version = "~> 0.12"
  required_providers {
    aws = {
      version = "~> 3.7"
    }
    random = {
      version = "~> 2.3"
    }
  }
}

provider "aws" {
  profile = module.vars.profile
  region  = module.vars.region
}

data "terraform_remote_state" "storage" {
  backend = "local"

  config = {
    path = "../storage/terraform.tfstate"
  }
}

module "vpc_main" {
  source     = "../modules/vpc"
  region     = module.vars.region
  tags       = module.vars.common_tags
  subnets    = module.vars.subnets
  cidr_block = module.vars.main_cidr
}

resource "aws_security_group" "ssh_from_internet" {
  name        = "SSH from Internet"
  description = "Security group that allows SSH and VPN traffic from Internet"
  vpc_id      = module.vpc_main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = module.vars.common_tags
}

resource "aws_security_group" "all_from_to_vpc" {
  name        = "All from/to VPC"
  description = "Default security group that allows inbound and outbound traffic between all instances in the VPC"
  vpc_id      = module.vpc_main.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [module.vpc_main.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [module.vpc_main.cidr_block]
  }

  tags = module.vars.common_tags
}

resource "aws_security_group" "all_to_internet" {
  name        = "All to Internet"
  description = "All traffic from instances to Internet"

  vpc_id = module.vpc_main.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "roles" {
  source       = "../modules/roles"
  org          = module.vars.org
  project_name = module.vars.project_name
  env          = module.vars.env
  region       = module.vars.region
  tags         = module.vars.common_tags
  buckets = [
    data.terraform_remote_state.storage.outputs.data_storage.bucket_arn,
    data.terraform_remote_state.storage.outputs.artifacts_storage.bucket_arn,
    data.terraform_remote_state.storage.outputs.metastore_storage.bucket_arn,
    data.terraform_remote_state.storage.outputs.logs_storage.bucket_arn
  ]
}

module "env_secrets" {
  source       = "../modules/secret"
  org          = module.vars.org
  project_name = module.vars.project_name
  env          = module.vars.env
  region       = module.vars.region
  tags         = module.vars.common_tags
}

module "hive_metastore_db" {
  source           = "../modules/db"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  tags             = module.vars.common_tags
  application_name = local.applications.hive_metastore_service
  subnet_ids       = module.vpc_main.subnet_ids
  security_groups = [
    aws_security_group.all_from_to_vpc.id
  ]
  master_user     = module.env_secrets.metastore_db_master_user
  master_password = module.env_secrets.metastore_db_master_password
}

# TODO: this is an unsafe, ec2 user data will have secrets in plaintext
# potential alternative: https://stackoverflow.com/a/46263044
data "template_file" "db_configurator_script_tpl" {
  template = file("${path.module}/db_configurator_script.tpl")
  vars = {
    db_host            = module.hive_metastore_db.host
    db_port            = module.hive_metastore_db.port
    master_user        = module.env_secrets.metastore_db_master_user
    master_password    = module.env_secrets.metastore_db_master_password
    metastore_user     = module.env_secrets.metastore_db_app_user
    metastore_password = module.env_secrets.metastore_db_app_password
  }
}

module "db_configurator" {
  source           = "../modules/instance"
  org              = module.vars.org
  project_name     = module.vars.project_name
  env              = module.vars.env
  region           = module.vars.region
  tags             = module.vars.common_tags
  application_name = local.applications.hive_metastore_provisioner
  subnet_id        = module.vpc_main.subnet_ids[0]
  vpc_security_group_ids = [
    aws_security_group.all_from_to_vpc.id
  ]
  entrypoint = data.template_file.db_configurator_script_tpl.rendered
}


# TODO: check whether I need this:
# https://aws.amazon.com/premiumsupport/knowledge-center/execute-user-data-ec2/
data "template_file" "hive_metastore_service_tpl" {
  template = file("${path.module}/hive-metastore.service.tpl")
  vars = {
    db_host                 = module.hive_metastore_db.host
    db_port                 = module.hive_metastore_db.port
    metastore_user          = module.env_secrets.metastore_db_app_user
    metastore_password      = module.env_secrets.metastore_db_app_password
    metastore_warehouse_dir = "s3://${data.terraform_remote_state.storage.outputs.metastore_storage.bucket_id}/metastore"
  }
}

module "hive_metastore_instances_group" {
  source               = "../modules/instances_group"
  org                  = module.vars.org
  project_name         = module.vars.project_name
  env                  = module.vars.env
  region               = module.vars.region
  tags                 = module.vars.common_tags
  application_name     = local.applications.hive_metastore_service
  vpc_id               = module.vpc_main.id
  subnet_ids           = module.vpc_main.subnet_ids
  port                 = "9083"
  key_name             = module.vars.ssh_key_name
  iam_instance_profile = module.roles.metastore_service_instance_profile
  # TODO: This is too permissive.
  # Consider https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html for accessing s3.
  vpc_security_group_ids = [
    aws_security_group.all_to_internet.id,
    aws_security_group.all_from_to_vpc.id
  ]
  entrypoint = data.template_file.hive_metastore_service_tpl.rendered
}
