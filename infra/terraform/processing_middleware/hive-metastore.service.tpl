#!/bin/bash

# Write systemd unit file
cat << EOF > /etc/systemd/system/hive-metastore.service
[Unit]
Description=Hive Metastore

[Service]
User=hive
Environment=HADOOP_HEAPSIZE=6144
ExecStart=/bin/metastore-entrypoint.sh start-metastore --hiveconf metastore.warehouse.dir=${metastore_warehouse_dir} --hiveconf javax.jdo.option.ConnectionURL=jdbc:mysql://${db_host}:${db_port}/metastore --hiveconf javax.jdo.option.ConnectionUserName=${metastore_user} --hiveconf javax.jdo.option.ConnectionPassword=${metastore_password}
Restart=always
RestartSec=15
StartLimitBurst=5

[Install]
WantedBy=multi-user.target
EOF

# Start systemd service
systemctl enable hive-metastore.service
systemctl start hive-metastore.service
