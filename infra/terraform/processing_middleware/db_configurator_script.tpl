#!/bin/bash

result=$(mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} -s -N -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='metastore'")

if [[ ! -z "$result" ]];
then
  echo "Database already exists. Shutting down."
  shutdown -h now
fi

/bin/metastore-entrypoint.sh schematool -initSchema -dbType mysql \
  -userName ${master_user} \
  -passWord ${master_password} \
  -url jdbc:mysql://${db_host}:${db_port}/metastore?createDatabaseIfNotExist=true

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "DROP USER '${metastore_user}'@'%';"

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "FLUSH PRIVILEGES;"

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "CREATE USER '${metastore_user}'@'%' IDENTIFIED BY '${metastore_password}';"

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "REVOKE ALL PRIVILEGES, GRANT OPTION FROM '${metastore_user}'@'%';"

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "GRANT ALL PRIVILEGES ON metastore.* TO '${metastore_user}'@'%';"

mysql -h ${db_host} -P ${db_port} -u ${master_user} -p${master_password} \
    -e "FLUSH PRIVILEGES;"

shutdown -h now
