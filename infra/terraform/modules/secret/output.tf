output "metastore_db_master_user" {
  value = random_password.metastore_db_master_user.result
}

output "metastore_db_master_password" {
  value = random_password.metastore_db_master_password.result
}

output "metastore_db_app_user" {
  value = random_password.metastore_db_app_user.result
}

output "metastore_db_app_password" {
  value = random_password.metastore_db_app_password.result
}
