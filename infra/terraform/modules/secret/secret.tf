resource "random_password" "metastore_db_master_user" {
  length  = 8
  special = false
  upper   = false
  number  = false
}

resource "random_password" "metastore_db_master_password" {
  length  = 32
  special = false
}

resource "random_password" "metastore_db_app_user" {
  length  = 8
  special = false
  upper   = false
  number  = false
}

resource "random_password" "metastore_db_app_password" {
  length  = 32
  special = false
}

resource "aws_secretsmanager_secret" "secret" {
  name_prefix = "${var.org}-${var.env}-${var.region}-${var.project_name}-"
  tags        = var.tags
}

resource "aws_secretsmanager_secret_version" "version" {
  secret_id = aws_secretsmanager_secret.secret.id
  secret_string = jsonencode(
    {
      metastore_db_master_user     = random_password.metastore_db_master_user.result
      metastore_db_master_password = random_password.metastore_db_master_password.result
      metastore_db_app_user        = random_password.metastore_db_app_user.result
      metastore_db_app_password    = random_password.metastore_db_app_password.result
    }
  )
}

