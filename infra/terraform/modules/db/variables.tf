variable "org" {}

variable "env" {}

variable "region" {}

variable "project_name" {}

variable "application_name" {}

variable "tags" {}

variable "subnet_ids" {}

variable "security_groups" { default = [] }

variable "instance_type" { default = "db.m5.large" }

variable "port" { default = 3306 }

variable "engine" { default = "mysql" }

variable "engine_version" { default = "5.7" }

variable "master_user" {}

variable "master_password" {}
