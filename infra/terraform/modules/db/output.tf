output "host" {
  value = aws_db_instance.db.address
}

output "port" {
  value = aws_db_instance.db.port
}
