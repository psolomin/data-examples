locals {
  suffix = "${var.org}-${var.env}-${var.region}-${var.project_name}-${var.application_name}"
}

resource "aws_db_subnet_group" "subnet_group" {
  name       = "db-subnet-group-${local.suffix}"
  subnet_ids = var.subnet_ids
  tags       = var.tags
}

resource "aws_db_parameter_group" "default" {
  name   = "db-params-${local.suffix}"
  family = "${var.engine}${var.engine_version}"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_instance" "db" {
  identifier             = "db-${local.suffix}"
  instance_class         = var.instance_type
  allocated_storage      = 64
  engine                 = var.engine
  engine_version         = var.engine_version
  db_subnet_group_name   = aws_db_subnet_group.subnet_group.name
  port                   = var.port
  username               = var.master_user
  password               = var.master_password
  skip_final_snapshot    = true
  tags                   = var.tags
  vpc_security_group_ids = var.security_groups
  parameter_group_name   = aws_db_parameter_group.default.name
}
