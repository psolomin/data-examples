locals {
  top_level_suffix    = "${var.org}-${var.application_name}"
  lb_resources_suffix = "${var.org}-${var.env}-${var.application_name}"
  suffix              = "${var.org}-${var.env}-${var.region}-${var.project_name}-${var.application_name}"
}

resource "aws_lb" "load_balancer" {
  name               = "lb-${local.lb_resources_suffix}"
  internal           = true
  load_balancer_type = "network"
  subnets            = var.subnet_ids
  tags               = var.tags
}

resource "aws_lb_target_group" "target_group" {
  name        = "lb-tg-${local.lb_resources_suffix}"
  port        = var.port
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = var.vpc_id

  health_check {
    interval            = 30
    protocol            = "TCP"
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = var.tags
}

resource "aws_lb_listener" "load_balancer_listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = var.port
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}

data "aws_ami" "packer_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ami-${local.top_level_suffix}-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["self"]
}

resource "aws_launch_configuration" "launch_configuration" {
  name_prefix                 = "launch-configuration-${local.suffix}-"
  image_id                    = data.aws_ami.packer_image.id
  instance_type               = var.instance_type
  security_groups             = var.vpc_security_group_ids
  associate_public_ip_address = true
  key_name                    = var.key_name
  user_data                   = var.entrypoint
  iam_instance_profile        = var.iam_instance_profile

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_placement_group" "placement_group" {
  name     = "placement-group-${local.suffix}"
  strategy = "partition"
}

resource "aws_autoscaling_group" "autoscaling_group" {
  # TODO: this is an unsafe work-around for deploying EC2s once AMI changes.
  # https://github.com/terraform-providers/terraform-provider-aws/issues/1276#issuecomment-398847342
  # https://github.com/terraform-providers/terraform-provider-aws/issues/13785#issue-640255009
  name_prefix               = "autoscaling-group-${aws_launch_configuration.launch_configuration.name}-"
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = 120
  health_check_type         = "EC2"
  # min_elb_capacity          = var.desired_size
  # wait_for_capacity_timeout = "15m"
  desired_capacity     = var.desired_size
  force_delete         = true
  placement_group      = aws_placement_group.placement_group.id
  launch_configuration = aws_launch_configuration.launch_configuration.name
  vpc_zone_identifier  = var.subnet_ids

  target_group_arns = [aws_lb_target_group.target_group.arn]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "ec2-${local.suffix}"
    propagate_at_launch = true
  }

  dynamic tag {
    for_each = var.tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}
