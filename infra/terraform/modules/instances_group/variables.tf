variable "org" {}

variable "env" {}

variable "region" {}

variable "project_name" {}

variable "application_name" {}

variable "tags" {}

variable "vpc_id" {}

variable "subnet_ids" {}

variable "port" {}

variable "entrypoint" { default = "echo 'hello'" }

variable "iam_instance_profile" {}

variable "vpc_security_group_ids" { default = null }

variable "instance_type" { default = "t3.large" }

variable "key_name" { default = null }

variable "min_size" { default = 1 }

variable "max_size" { default = 3 }

variable "desired_size" { default = 1 }
