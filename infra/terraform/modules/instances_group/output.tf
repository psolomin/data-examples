output "host" {
  value = aws_lb.load_balancer.dns_name
}

output "port" {
  value = aws_lb_listener.load_balancer_listener.port
}
