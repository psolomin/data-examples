variable "org" {}

variable "env" {}

variable "region" {}

variable "project_name" {}

variable "application_name" {}

variable "metastore_host" {}

variable "metastore_port" {}

variable "tags" {}

variable "subnet_id" {}

variable "emr_role" {}

variable "emr_autoscaling_role" {}

variable "ec2_profile" {}

variable "auto_terminate" { default = false }

variable "log_base_uri" {}

variable "vpc_id" {}

variable "additional_security_group_ids" { default = null }

variable "release" { default = "emr-5.31.0" }

variable "applications" { default = ["Hive", "Spark"] }

variable "steps" { default = [] }

variable "instance_type" { default = "m5.xlarge" }

variable "key_name" { default = null }

variable "min_workers" { default = 1 }

variable "max_workers" { default = 2 }
