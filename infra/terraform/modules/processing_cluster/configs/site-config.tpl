[
  {
    "Classification": "hive-site",
    "Properties": {
      "hive.metastore.uris": "thrift://${METASTORE_HOST}:${METASTORE_PORT}"
    }
  },
  {
    "Classification": "spark-hive-site",
    "Properties": {
      "hive.metastore.uris": "thrift://${METASTORE_HOST}:${METASTORE_PORT}"
    }
  },
  {
    "Classification": "presto-connector-hive",
    "Properties": {
      "connector.name": "hive-hadoop2",
      "hive.metastore.uri": "thrift://${METASTORE_HOST}:${METASTORE_PORT}"
    }
  }
]
