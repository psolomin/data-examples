locals {
  suffix = "${var.org}-${var.env}-${var.region}-${var.project_name}-${var.application_name}"
}

data "template_file" "autoscaling_policy_tpl" {
  template = templatefile(
    "${path.module}/configs/autoscaling-policy.tpl",
    {
      MIN_WORKERS = var.min_workers,
      MAX_WORKERS = var.max_workers
    }
  )
}

data "template_file" "site_config_tpl" {
  template = templatefile(
    "${path.module}/configs/site-config.tpl",
    {
      METASTORE_HOST = var.metastore_host,
      METASTORE_PORT = var.metastore_port
    }
  )
}

resource "aws_security_group" "sg_master" {
  name                   = "SG-Master-${local.suffix}"
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = true
  tags                   = var.tags

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    ignore_changes = [ingress, egress]
  }
}

resource "aws_security_group" "sg_workers" {
  name                   = "SG-Worker-${local.suffix}"
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = true
  tags                   = var.tags

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    ignore_changes = [ingress, egress]
  }
}

# TODO: configurations_json should be injected from outside
resource "aws_emr_cluster" "cluster" {
  name                              = local.suffix
  release_label                     = var.release
  applications                      = var.applications
  service_role                      = var.emr_role
  autoscaling_role                  = var.emr_autoscaling_role
  termination_protection            = false
  keep_job_flow_alive_when_no_steps = ! var.auto_terminate
  log_uri                           = "${var.log_base_uri}/${local.suffix}/log/"
  configurations_json               = data.template_file.site_config_tpl.rendered
  visible_to_all_users              = true

  ec2_attributes {
    subnet_id                         = var.subnet_id
    emr_managed_master_security_group = aws_security_group.sg_master.id
    emr_managed_slave_security_group  = aws_security_group.sg_workers.id
    additional_master_security_groups = join(", ", var.additional_security_group_ids)
    additional_slave_security_groups  = join(", ", var.additional_security_group_ids)
    instance_profile                  = var.ec2_profile
    key_name                          = var.key_name
  }

  master_instance_group {
    name           = "emr-master-${local.suffix}"
    instance_type  = var.instance_type
    instance_count = 1
    ebs_config {
      size                 = "20"
      type                 = "gp2"
      volumes_per_instance = 2
    }
  }

  core_instance_group {
    name           = "emr-worker-${local.suffix}"
    instance_type  = var.instance_type
    instance_count = var.min_workers
    ebs_config {
      size                 = "100"
      type                 = "gp2"
      volumes_per_instance = 2
    }
    autoscaling_policy = data.template_file.autoscaling_policy_tpl.rendered
  }

  step {
    action_on_failure = "TERMINATE_CLUSTER"
    name              = "Setup Hadoop Debugging"

    hadoop_jar_step {
      jar  = "command-runner.jar"
      args = ["state-pusher-script"]
    }
  }

  dynamic "step" {
    for_each = [for s in var.steps : {
      name = s.name
      jar  = s.jar
      args = s.args
    }]

    content {
      action_on_failure = "TERMINATE_CLUSTER"
      name              = step.value.name

      hadoop_jar_step {
        jar  = step.value.jar
        args = step.value.args
      }
    }
  }

  tags = var.tags
}
