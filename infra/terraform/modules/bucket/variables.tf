variable "org" {}

variable "env" {}

variable "region" {}

variable "project_name" {}

variable "tags" {}

variable "application_name" {}
