resource "aws_s3_bucket" "bucket" {
  bucket        = "${var.org}-${var.env}-${var.region}-${var.project_name}-${var.application_name}"
  acl           = "private"
  tags          = var.tags
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "raw" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
