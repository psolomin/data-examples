locals {
  # TODO: modify policies such that they are not too permissive
  suffix  = "${var.org}-${var.env}-${var.project_name}"
  buckets = jsonencode(var.buckets)
  objects = jsonencode([for b in var.buckets : "${b}/*"])
}

data "template_file" "emr_assume_role_policy_tpl" {
  template = templatefile("${path.module}/policies/assume-role-policy.tpl", {})
}

data "template_file" "emr_role_policy_tpl" {
  template = templatefile(
    "${path.module}/policies/emr-role-policy.tpl",
    { BUCKETS = local.buckets, OBJECTS = local.objects }
  )
}

data "template_file" "emr_autoscaling_role_policy_tpl" {
  template = templatefile("${path.module}/policies/emr-autoscaling-role-policy.tpl", {})
}

/*
EMR role spec
*/
resource "aws_iam_policy" "emr_role_policy" {
  name   = "emr-role-policy-${local.suffix}"
  path   = "/"
  policy = data.template_file.emr_role_policy_tpl.rendered
}

resource "aws_iam_role" "emr_role" {
  name               = "emr-role-${local.suffix}"
  assume_role_policy = data.template_file.emr_assume_role_policy_tpl.rendered
  tags               = var.tags
}

resource "aws_iam_role_policy_attachment" "emr_role_policy_attachment" {
  role       = aws_iam_role.emr_role.name
  policy_arn = aws_iam_policy.emr_role_policy.arn
}

/*
EMR EC2 instance profile role
*/
resource "aws_iam_role" "emr_ec2_profile_role" {
  name               = "emr-ec2-profile-role-${local.suffix}"
  assume_role_policy = data.template_file.emr_assume_role_policy_tpl.rendered
  tags               = var.tags
}

resource "aws_iam_instance_profile" "emr_ec2_profile" {
  name = "emr-ec2-profile-${local.suffix}"
  role = aws_iam_role.emr_ec2_profile_role.name
}

data "template_file" "emr_ec2_profile_policy_tpl" {
  template = templatefile(
    "${path.module}/policies/ec2-profile-policy.tpl",
    { BUCKETS = local.buckets, OBJECTS = local.objects }
  )
}

resource "aws_iam_role_policy" "emr_ec2_profile_policy" {
  name   = "emr-ec2-profile-policy-${local.suffix}"
  role   = aws_iam_role.emr_ec2_profile_role.id
  policy = data.template_file.emr_ec2_profile_policy_tpl.rendered
}

/*
EMR Autoscaling role
*/
resource "aws_iam_policy" "emr_autoscaling_role_policy" {
  name   = "emr-autoscaling-role-policy-${local.suffix}"
  path   = "/"
  policy = data.template_file.emr_autoscaling_role_policy_tpl.rendered
}

resource "aws_iam_role" "emr_autoscaling_role" {
  name               = "emr-autoscaling-role-${local.suffix}"
  assume_role_policy = data.template_file.emr_assume_role_policy_tpl.rendered
  tags               = var.tags
}

resource "aws_iam_role_policy_attachment" "emr_autoscaling_role_policy_attachment" {
  role       = aws_iam_role.emr_autoscaling_role.name
  policy_arn = aws_iam_policy.emr_autoscaling_role_policy.arn
}

/*
Hive Metastore EC2 instance profile
*/
resource "aws_iam_role" "metastore_service_ec2_profile_role" {
  name               = "metastore_service-profile-role-${local.suffix}"
  assume_role_policy = data.template_file.emr_assume_role_policy_tpl.rendered
  tags               = var.tags
}

resource "aws_iam_instance_profile" "metastore_service_ec2_profile" {
  name = "metastore_service-profile-${local.suffix}"
  role = aws_iam_role.metastore_service_ec2_profile_role.name
}

data "template_file" "metastore_service_ec2_profile_policy_tpl" {
  template = templatefile(
    "${path.module}/policies/ec2-profile-policy.tpl",
    { BUCKETS = local.buckets, OBJECTS = local.objects }
  )
}

resource "aws_iam_role_policy" "metastore_service_ec2_profile_policy" {
  name   = "metastore_service-ec2-profile-policy-${local.suffix}"
  role   = aws_iam_role.metastore_service_ec2_profile_role.id
  policy = data.template_file.metastore_service_ec2_profile_policy_tpl.rendered
}
