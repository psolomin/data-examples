output "emr_roles" {
  value = {
    emr_role             = aws_iam_role.emr_role.arn
    emr_ec2_role         = aws_iam_role.emr_ec2_profile_role.arn
    emr_ec2_profile      = aws_iam_instance_profile.emr_ec2_profile.arn
    emr_autoscaling_role = aws_iam_role.emr_autoscaling_role.arn
  }
}

output "metastore_service_instance_profile" {
  value = aws_iam_instance_profile.metastore_service_ec2_profile.arn
}
