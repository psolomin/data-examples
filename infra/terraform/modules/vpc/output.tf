output "id" {
  value = aws_vpc.vpc.id
}

output "cidr_block" {
  value = aws_vpc.vpc.cidr_block
}

output "subnet_ids" {
  value = [for subnet in aws_subnet.subnet : subnet.id]
}

output "main_routing_table" {
  value = aws_default_route_table.route_table
}
