variable "org" {}

variable "env" {}

variable "region" {}

variable "project_name" {}

variable "application_name" {}

variable "entrypoint" {}

variable "tags" {}

variable "subnet_id" {}

variable "instance_type" { default = "m3.medium" }

variable "key_name" { default = null }

variable "vpc_security_group_ids" { default = null }
