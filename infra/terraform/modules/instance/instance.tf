locals {
  top_level_suffix = "${var.org}-${var.application_name}"
  suffix           = "${var.org}-${var.env}-${var.region}-${var.project_name}-${var.application_name}"
}

data "aws_ami" "packer_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ami-${local.top_level_suffix}-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["self"]
}

resource "aws_instance" "ec2_instance" {
  ami                         = data.aws_ami.packer_image.id
  instance_type               = var.instance_type
  associate_public_ip_address = true
  subnet_id                   = var.subnet_id
  user_data                   = var.entrypoint
  tags = merge(
    var.tags,
    { Name : "ec2-${local.suffix}" }
  )
  key_name                             = var.key_name
  vpc_security_group_ids               = var.vpc_security_group_ids
  instance_initiated_shutdown_behavior = "terminate"
}
