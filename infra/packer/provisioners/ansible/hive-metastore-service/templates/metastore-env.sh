export JAVA_HOME=/etc/alternatives/jre_1.8.0_openjdk
export HADOOP_HOME={{ var_hadoop_home }}
export HADOOP_CLASSPATH=${HADOOP_HOME}/share/hadoop/tools/lib/aws-java-sdk-bundle-1.11.375.jar:${HADOOP_HOME}/share/hadoop/tools/lib/hadoop-aws-{{ var_hadoop_version }}.jar
export METASTORE_HOME={{ var_metastore_home }}
